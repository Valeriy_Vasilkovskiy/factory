package main;

public class Cars implements StatsAndOperationWithCarOrBoat{
    private String model;

    public Cars(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    @Override
    public void printName() {
        System.out.println(getModel());
    }

    @Override
    public void printSize() {
        switch (getModel()){
            case "Audi":
                System.out.println("220 X 300");
                break;
            case "Lanos":
                System.out.println("200 X 280");
        }
    }

    @Override
    public void looksMaxSpeed() {

    }
}
