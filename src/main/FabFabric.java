package main;

class FabFabric{
    private static FabFabric  inst;

    public FabFabric() {

    }
    public static FabFabric getInst() {
        if (inst == null) {
            return inst = new FabFabric();
        } else {
            return inst;
        }
    }
    public FirstFabrics CarOrBoat(int key) {
        switch (key) {
            case 1:
                return new FabricCar();
            case 2:
                return new FabricBoat();
        }
        return null;
    }
}
