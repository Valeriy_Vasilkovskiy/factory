package main;

public class Boats implements StatsAndOperationWithCarOrBoat {
    private String model;

    public Boats(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    @Override
    public void printName() {
        System.out.println(getModel());
    }

    @Override
    public void printSize() {

    }

    @Override
    public void looksMaxSpeed() {

    }
}
